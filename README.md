# WR Cloud ARM Deployment

Below is a collection of resources and documentation on the deployment of the WR Cloud environment using ARM templates.

# Helpful Resources

[ARM Overview](https://docs.microsoft.com/en-gb/azure/azure-resource-manager/management/overview)

[ARM Template Overview](https://docs.microsoft.com/en-gb/azure/azure-resource-manager/templates/overview)

[Linked Templates Tutorial](https://docs.microsoft.com/en-us/azure/azure-resource-manager/templates/linked-templates)

[Azure CLI](https://docs.microsoft.com/en-us/cli/azure/?view=azure-cli-latest)

[Azure Architecture Center](https://docs.microsoft.com/en-us/azure/architecture/)

[Azure Resource Manager template documentation](https://docs.microsoft.com/en-us/azure/azure-resource-manager/templates/)

# Documentation

The following is the outline of the order in which resources must be created, including their dependencies on other resources, and notes on the deployment. 

## 1.	Key Vault 

### Dependencies:
a.	Access Policies requires an AAD object ID for an App Registration
b.	Access Policies requires an AAD object ID for principle user (to manage) - also from AAD
c.	A certificate to authenticate administrator for service fabric – deployment of application and service fabric explorer
d.	7 secrets – Fabric, CentralDB, Journals, JournalsSQL, Graph , Log, Redis

Notes:
Azure Key Vault names are universally unique so if one exists in one resource group it cannot be created in another by anyone else in the world unless the endpoint to vault differs by regions e.g. 
mrpi-cbe-dev-kyv-01.vault.azure.net created in Resource Group MRPI-Sandbox cannot be created in CBE-ARM-Test because both resources would evaluate the URL to mrpi-cbe-deve-kyv-01.vault.azure.net.

### Template Details
**Parameters:** keyVaultName, location, sku, accessPolicies, tenant, enabledForDeployment, enabledForTemplateDeployment, enabledForDiskEncryption, networkAcls, resourceTags

**Variables:** 

**User-defined functions:** 

**Resources:** Microsoft.KeyVault/vaults

**Outputs:** 
certificateUrl, 

### Exceptions 
List any network exceptions that need to be made to allow communication to this resource.

### References

[Managing Key Vault with Azure CLI](https://docs.microsoft.com/en-us/azure/key-vault/key-vault-manage-with-cli2)


## 2.	Service Fabric 
### Dependencies:
**Parameters:** 
certificateUrl (KeyVault output), 

**Variables:** 

**User-defined functions:** 

**Resources:** Microsoft.KeyVault/vaults

**Outputs:** 
Output:
Storage accounts

### References

[Setting up cluster with ARM](https://docs.microsoft.com/en-us/azure/service-fabric/service-fabric-cluster-creation-via-arm)

## 3.	SQL Server
### Dependencies:
**Parameters:** 
serverName, administratorLogin, administratorLoginPassword

**Variables:** 

**User-defined functions:** 

**Resources:** 

**Outputs:** 

### References

## 4.	SQL Database - Central
### Dependencies:
**Parameters:** 

**Variables:** 

**User-defined functions:** 

**Resources:** 

**Outputs:** 

### References

## 5.	SQL Database - Journals
### Dependencies:
**Parameters:** 

**Variables:** 

**User-defined functions:** 

**Resources:** 

**Outputs:** 

### References
